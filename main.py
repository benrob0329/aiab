from kivy.app import App
from kivy.core.audio import SoundLoader
from kivy.clock import Clock

from kivy.uix.boxlayout import BoxLayout
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.popup import Popup

from kivy.properties import BoundedNumericProperty, ObjectProperty

from math import ceil
import time, PIL, os, json

### In: Audio & Pictures
## Get Audio
# Get Length And Create Frames Dict
## Scan 'assets' dir
### Display Current Frame & Info
## Display Switchers For Each Layer
## Bind Switchers To Current Frame
## Display Layer Stack From Switcher Values
### Out: Image Sequence
## Loop Through Frames Dict
## Save Each Frame


# Dialog Class For File Loading
class LoadDialog(FloatLayout):
    load = ObjectProperty(None)
    cancel = ObjectProperty(None)


# Some functions needed to be here so that they could access the ids dictionary
class RootWidget(BoxLayout):

    # Sets the current framenum
    def set_frame(self, x):
        x = str(x)
        try:
            app.framenum = int(x)
        except ValueError:
            return

        app.stop_sound()

        # Then updated the Layer Spinners witht the current information
        if x in app.frames:
            self.ids.bro.text = app.frames[x]['bro']
            self.ids.ben.text = app.frames[x]['ben']
            # Or copy the current state to the new frame
        else:
            app.frames[x] = ({'bro': self.ids.bro.text,
                              'ben': self.ids.ben.text})

    def render(self):
        # Makes the render directory if it doesn't exist
        try:
            os.mkdir(app.path + '/render')
        except FileExistsError:
            print('render directory exists, continuing')
                
        # Loops through the frames dictionary
        # Compositing the layers together before saving it to the render directory
        for key, value in app.frames.items():
            im = PIL.Image.open('assets/background/main.png')
            im = PIL.Image.alpha_composite(im, PIL.Image.open('assets/bro/' + value['bro']))
            im = PIL.Image.alpha_composite(im, PIL.Image.open('assets/ben/' + value['ben']))
            im.save(app.path + '/render/' + key + '.png', 'PNG')
                
    def save(self):
        # Open the file '<audio file directory>/aiab.json'
        with open(app.path + '/aiab.json', 'w') as f:
            # Combine the frames and audio path into a single dictionary, then save to f
            savedict = {'frames' : app.frames, 'audio' : app.sound.source}
            f.write(json.dumps(savedict))


class AiabApp(App):
    # First Define some defaults
    sound = None
    frames = {}
    framenum = BoundedNumericProperty(0, min=0, max=0)
    path = ""

    def load(self, path, infile):
        self.dismiss_popup()
            
        # Define the App variable 'path' as the one from the fileselection screen
        self.path = path

        # If the file is a project dump, set the frames variable and audio filename
        if 'aiab.json' in infile[0]:
            with open(infile[0]) as f:
                load = f.read()
                load = json.loads(load)
                self.frames = load['frames']
                audio = load['audio']
                # Else start a new project, leaving frames at the default
        else:
            audio = infile[0]
                
        # Load the sound file
        self.sound = SoundLoader.load(audio)

        # If sound loaded correctly, set the number of frames to
        # be equal to 4fp (rounded up)
        if self.sound:
            self.property('framenum').set_max(self, ceil(self.sound.length * 0.25))
                
    def play_sound(self, frame):
        # Play, then seek to the current time
        self.sound.play()
        self.sound.seek(4 * int(frame))
        # Stop the sound after 4 seconds
        Clock.schedule_once(app.stop_sound, 4)
        
    def stop_sound(self, dt=1):
        self.sound.stop()

    def dismiss_popup(self):
        self._popup.dismiss()
            
    def show_load(self, dt=1):
        # Create the file chooser instance
        content = LoadDialog(load=self.load, cancel=quit)
        # Create and show a popup for the file chooser
        self._popup = Popup(title="Load File", content=content, size_hint=(0.9, 0.9))
        self._popup.open()


if __name__ == '__main__':
    app = AiabApp()
    # Open a file first thing
    Clock.schedule_once(app.show_load, 0)
    app.run()
